﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Storm.Mvvm;
using Fourplaces.V1.Views;
using Fourplaces.V1.Services;

namespace Fourplaces
{
    public partial class App : MvvmApplication
    {
        public App() : base(() => new LoginPage())
        {
            InitializeComponent();

            // Enregistrement des services
            DependencyService.Register<IApiClientService, ApiClientService>();
            DependencyService.Register<IManagerService, ManagerService>();
            DependencyService.Register<IPermissionsService, PermissionsService>();
        }
    }
}
