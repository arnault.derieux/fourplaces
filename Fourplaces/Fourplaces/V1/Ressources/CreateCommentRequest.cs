using Newtonsoft.Json;

namespace Fourplaces.V1.Ressources
{
	public class CreateCommentRequest
	{
		[JsonProperty("text")]
		public string Text { get; set; }
	}
}