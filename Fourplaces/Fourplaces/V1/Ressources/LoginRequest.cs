using Newtonsoft.Json;

namespace Fourplaces.V1.Ressources
{
    public class LoginRequest
    {
        [JsonProperty("email")]
        public string Email { get; set; }
        
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}