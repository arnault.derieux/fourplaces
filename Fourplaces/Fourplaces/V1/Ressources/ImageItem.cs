using Newtonsoft.Json;

namespace Fourplaces.V1.Ressources
{
	public class ImageItem
	{
		[JsonProperty("id")]
		public int Id { get; set; }
	}
}