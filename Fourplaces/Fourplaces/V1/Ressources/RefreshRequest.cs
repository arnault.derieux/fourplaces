using Newtonsoft.Json;

namespace Fourplaces.V1.Ressources
{
    public class RefreshRequest
    {
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}