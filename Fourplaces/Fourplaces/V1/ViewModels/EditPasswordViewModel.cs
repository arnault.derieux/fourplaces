﻿using Fourplaces.V1.Ressources;
using Fourplaces.V1.Services;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces.V1.ViewModels
{
    class EditPasswordViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<INavigationService>   _navigationService;
        private readonly Lazy<IDialogService>       _dialogService;
        private readonly Lazy<IManagerService>      _managerService;

        // Variables
        private string _passwordOld;
        private string _passwordNew;

        // Binding
        public string PasswordOld
        {
            get => _passwordOld;
            set => SetProperty(ref _passwordOld, value);
        }

        public string PasswordNew
        {
            get => _passwordNew;
            set => SetProperty(ref _passwordNew, value);
        }

        // Commands
        public ICommand EditCommand { get; }

        // Constructeur
        public EditPasswordViewModel()
        {
            _navigationService  = new Lazy<INavigationService>(()   => DependencyService.Resolve<INavigationService>());
            _dialogService      = new Lazy<IDialogService>(()       => DependencyService.Resolve<IDialogService>());
            _managerService     = new Lazy<IManagerService>(()      => DependencyService.Resolve<IManagerService>());

            EditCommand = new Command(EditAction);
        }

        private async void EditAction(object obj)
        {
            if(PasswordNew != null && PasswordOld != null)
            {
                UpdatePasswordRequest res = new UpdatePasswordRequest() { NewPassword = PasswordNew, OldPassword = PasswordOld };
                Response response = await _managerService.Value.UpdatePasswordUser(res);
                if (response.IsSuccess)
                {
                    await _navigationService.Value.PopAsync();
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Erreur lors de la requete", "Veuillez vérifier vos saisies.", "Ok");
                }
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Saisie invalide", "Veuillez vérifier vos saisies.", "Ok");
            }
        }
    }
}
