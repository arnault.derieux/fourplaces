﻿using Fourplaces.V1.Ressources;
using Fourplaces.V1.Services;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces.V1.ViewModels
{
    class EditProfilViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<INavigationService>   _navigationService;
        private readonly Lazy<IDialogService>       _dialogService;
        private readonly Lazy<IManagerService>      _managerService;
        private readonly Lazy<IPermissionsService>  _permService;

        // Variables
        private string  _firstname;
        private string  _lastname;
        private string  _labelChoose;
        private int     _idImage;
        private bool    _chooseEnable;
        private bool    _editEnable;

        // Commands
        public ICommand ChooseCommand { get; }
        public ICommand AddCommand { get; }

        // Commands
        public bool EditEnable
        {
            get => _editEnable;
            set => SetProperty(ref _editEnable, value);
        }
        public string LabelChoose
        {
            get => _labelChoose;
            set => SetProperty(ref _labelChoose, value);
        }
        public bool ChooseEnable
        {
            get => _chooseEnable;
            set => SetProperty(ref _chooseEnable, value);
        }
        public string Firstname
        {
            get => _firstname;
            set => SetProperty(ref _firstname, value);
        }
        public string Lastname
        {
            get => _lastname;
            set => SetProperty(ref _lastname, value);
        }
        public int ImageId
        {
            get => _idImage;
            set => SetProperty(ref _idImage, value);
        }

        // Constructeur
        public EditProfilViewModel()
        {
            _navigationService  = new Lazy<INavigationService>(()   => DependencyService.Resolve<INavigationService>());
            _dialogService      = new Lazy<IDialogService>(()       => DependencyService.Resolve<IDialogService>());
            _managerService     = new Lazy<IManagerService>(()      => DependencyService.Resolve<IManagerService>());
            _permService        = new Lazy<IPermissionsService>(()  => DependencyService.Resolve<IPermissionsService>());

            _ = FetchUser();

            AddCommand      = new Command(AddAction);
            ChooseCommand   = new Command(ChooseAction);

            ChooseEnable    = true;
            EditEnable       = true;
            LabelChoose     = "Ajouter/Changer une photo";
        }

        private async void ChooseAction()
        {
            ChooseEnable = false;
            var result = await _dialogService.Value.DisplayAlertAsync("Choix", "Veuillez chosir entre prendre une photo ou choisir une photo depuis la galerie", "Galerie", "Photo");
            if (result)
            {
                await CrossMedia.Current.Initialize();

                bool granted = await _permService.Value.Check(new string[1] { "Storage" });

                if (granted)
                {
                    EditEnable = false;
                    LabelChoose = "Ajout en cours...";
                    MediaFile file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                    {
                        // On reduit pour que ça passe dans la requete...
                        CompressionQuality = 10
                    });

                    if (file == null)
                    {
                        LabelChoose = "Ajouter/Changer une photo";
                        EditEnable = true;
                        ChooseEnable = true;
                        return;
                    }

                    ImageId = await _managerService.Value.PushImage(ConvertMediaFileToByteArray(file));
                    EditEnable = true;
                    LabelChoose = "Photo sélectionnée !";
                }
                else
                {
                    ChooseEnable = true;
                    await _dialogService.Value.DisplayAlertAsync("Permissions", "Permission non accordée.", "Ok");
                }
            }
            else
            {
                await CrossMedia.Current.Initialize();

                bool granted = await _permService.Value.Check(new string[2] { "Storage", "Camera" });

                if (granted)
                {

                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await _dialogService.Value.DisplayAlertAsync("Camera indisponible", "pas de caméra sur l'appareil.", "Ok");
                        return;
                    }
                    EditEnable = false;
                    LabelChoose = "Ajout en cours...";
                    MediaFile file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        Directory = "Sample",
                        Name = "test.jpg",
                        // On reduit pour que ça passe dans la requete...
                        CompressionQuality = 10
                    });

                    if (file == null)
                    {
                        LabelChoose = "Ajouter/Changer une photo";
                        EditEnable = true;
                        ChooseEnable = true;
                        return;
                    }

                    ImageId = await _managerService.Value.PushImage(ConvertMediaFileToByteArray(file));
                    EditEnable = true;
                    LabelChoose = "Photo sélectionnée !";
                }
                else
                {
                    ChooseEnable = true;
                    await _dialogService.Value.DisplayAlertAsync("Permissions", "Permission non accordée.", "Ok");
                }
            }
        }
        private byte[] ConvertMediaFileToByteArray(MediaFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                file.GetStream().CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public async Task FetchUser()
        {
            await base.OnResume();
            var userItem = await _managerService.Value.GetUser();
            Firstname = userItem.FirstName;
            Lastname = userItem.LastName;
            ImageId = (int)userItem.ImageId;
        }

        private async void AddAction()
        {
            if (Firstname != null && Lastname != null)
            {
                EditEnable = false;
                ChooseEnable = false;
                UpdateProfileRequest res;
                res = new UpdateProfileRequest() { FirstName = Firstname, LastName = Lastname, ImageId = ImageId};

                Response<UserItem> response = await _managerService.Value.UpdateUser(res);
                if (response.IsSuccess)
                {
                    await _navigationService.Value.PopAsync();
                }
                else
                {
                    EditEnable = true;
                    ChooseEnable = true;
                }
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Saisie invalide", "Veuillez vérifier vos saisies.", "Ok");
            }
        }
    }
}
