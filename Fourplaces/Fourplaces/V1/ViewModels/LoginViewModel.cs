﻿using Fourplaces.V1.Ressources;
using Fourplaces.V1.Services;
using Fourplaces.V1.Views;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Net.Http;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces.V1.ViewModels
{
    class LoginViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<IApiClientService>     _clientService;
        private readonly Lazy<INavigationService>    _navigationService;
        private readonly Lazy<IDialogService>        _dialogService;
        private readonly Lazy<IManagerService>       _managerService;
        private readonly Lazy<IPermissionsService>   _permService;

        // Constante URLs
        private const String ROOT       = "https://td-api.julienmialon.com";
        private const String LOGIN      = "/auth/login";
        private const String REGISTER   = "/auth/register";

        // Binding SignIn
        private string _emailSI;
        private string _passwordSI;

        // Binding SignUp
        private string _emailSU;
        private string _firstnameSU;
        private string _lastnameSU;
        private string _passwordSU;

        // Binding Visible
        private bool _blockSignInVisible = true;
        private bool _blockSignUpVisible = false;

        public string EmailSI
        {
            get => _emailSI;
            set => SetProperty(ref _emailSI, value);
        }

        public string PasswordSI
        {
            get => _passwordSI;
            set => SetProperty(ref _passwordSI, value);
        }

        public string EmailSU
        {
            get => _emailSU;
            set => SetProperty(ref _emailSU, value);
        }

        public string FirstnameSU
        {
            get => _firstnameSU;
            set => SetProperty(ref _firstnameSU, value);
        }
        public string LastnameSU
        {
            get => _lastnameSU;
            set => SetProperty(ref _lastnameSU, value);
        }

        public string PasswordSU
        {
            get => _passwordSU;
            set => SetProperty(ref _passwordSU, value);
        }

        public bool BlockSignInVisible
        {
            get => _blockSignInVisible;
            set => SetProperty(ref _blockSignInVisible, value);
        }

        public bool BlockSignUpVisible
        {
            get => _blockSignUpVisible;
            set => SetProperty(ref _blockSignUpVisible, value);
        }

        // Commands des bouttons
        public ICommand SignInCommand { get; }
        public ICommand SignUpCommand { get; }
        public ICommand SendingSignUpCommand { get; }
        public ICommand BackSignInCommand { get; }

        // Constructeur
        public LoginViewModel()
        {
            _clientService      = new Lazy<IApiClientService>(()    => DependencyService.Resolve<IApiClientService>());
            _navigationService  = new Lazy<INavigationService>(()   => DependencyService.Resolve<INavigationService>());
            _dialogService      = new Lazy<IDialogService>(()       => DependencyService.Resolve<IDialogService>());
            _managerService     = new Lazy<IManagerService>(()      => DependencyService.Resolve<IManagerService>());
            _permService        = new Lazy<IPermissionsService>(()  => DependencyService.Resolve<IPermissionsService>());

            SignInCommand           = new Command(LoginAction);
            SignUpCommand           = new Command(SignUpAction);
            SendingSignUpCommand    = new Command(SendingSignUpCommandAction);
            BackSignInCommand       = new Command(BackSignInAction);
        }

        private async void LoginAction()
        {
            _permService.Value.CheckAndAskAllPermAtStart();
            if (EmailSI != null && PasswordSI != null)
            {
                LoginRequest _data                          = new LoginRequest() { Email = EmailSI, Password = PasswordSI };
                HttpResponseMessage _httpResponseMessage    = await _clientService.Value.Execute(HttpMethod.Post, ROOT + LOGIN, _data);
                if (_httpResponseMessage.IsSuccessStatusCode)
                {
                    Response<LoginResult> _res = await _clientService.Value.ReadFromResponse<Response<LoginResult>>(_httpResponseMessage);
                    if (_res.IsSuccess)
                    {
                        _managerService.Value.Connect(_res.Data);
                        GoToHome();
                    }
                    else
                    {
                        await _dialogService.Value.DisplayAlertAsync("Echec de la connection", $"Connection échouée. Veuillez vérifier vos saisies et réessayer. Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                    }
                }
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Saisie invalide", "Veuillez vérifier vos saisies.", "Ok");
            }
        }
        private void SignUpAction()
        {
            BlockSignInVisible = false;
            BlockSignUpVisible = true;
        }

        private async void SendingSignUpCommandAction()
        {
            if(EmailSU != null && FirstnameSU != null && LastnameSU != null && PasswordSU != null)
            {
                RegisterRequest _data                       = new RegisterRequest() { Email = EmailSU, Password = PasswordSU, FirstName = FirstnameSU, LastName = LastnameSU };
                HttpResponseMessage _httpResponseMessage    = await _clientService.Value.Execute(HttpMethod.Post, ROOT + REGISTER, _data);
                if (_httpResponseMessage.IsSuccessStatusCode)
                {
                    Response<LoginResult> _res = await _clientService.Value.ReadFromResponse<Response<LoginResult>>(_httpResponseMessage);
                    if (_res.IsSuccess)
                    {
                        _managerService.Value.Connect(_res.Data);
                        GoToHome();
                    }
                    else
                    {
                        await _dialogService.Value.DisplayAlertAsync("Echec de l'enregistrement", $"Enregistrement échoué. Veuillez vérifier vos saisies et réessayer. Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                    }
                }
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Saisie invalide", "Veuillez vérifier vos saisies.", "Ok");
            }
        }

        private void BackSignInAction()
        {
            BlockSignUpVisible = false;
            BlockSignInVisible = true;
        }

        private async void GoToHome()
        {
            await _navigationService.Value.PushAsync<HomePage>();
        }
    }
}
