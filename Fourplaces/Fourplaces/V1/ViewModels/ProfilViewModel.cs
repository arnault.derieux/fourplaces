﻿using Fourplaces.V1.Services;
using Fourplaces.V1.Views;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces.V1.ViewModels
{
    class ProfilViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<INavigationService>   _navigationService;
        private readonly Lazy<IManagerService>      _managerService;

        // Variables
        private string  _email;
        private string  _firstname;
        private string  _lastname;
        private int     _imageId;

        // Commands
        public ICommand EditCommand { get; }
        public ICommand PasswordCommand { get; }

        // Bindings
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }
        public string Firstname
        {
            get => _firstname;
            set => SetProperty(ref _firstname, value);
        }
        public string Lastname
        {
            get => _lastname;
            set => SetProperty(ref _lastname, value);
        }
        public int ImageId
        {
            get => _imageId;
            set => SetProperty(ref _imageId, value);
        }

        // Constructeur
        public ProfilViewModel()
        {
            _navigationService  = new Lazy<INavigationService>(()   => DependencyService.Resolve<INavigationService>());
            _managerService     = new Lazy<IManagerService>(()      => DependencyService.Resolve<IManagerService>());

            _ = FetchUser();

            EditCommand     = new Command(EditAction);
            PasswordCommand = new Command(PasswordAction);
        }

        private async void PasswordAction(object obj)
        {
            await _navigationService.Value.PushAsync<EditPasswordPage>();
        }

        private async void EditAction(object obj)
        {
            await _navigationService.Value.PushAsync<EditProfilPage>();
        }

        public async Task FetchUser()
        {
            await base.OnResume();
            var userItem = await _managerService.Value.GetUser();
            Email = userItem.Email;
            Firstname = userItem.FirstName;
            Lastname = userItem.LastName;
            ImageId = (int)userItem.ImageId;
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            _ = FetchUser();
        }
    }
}
