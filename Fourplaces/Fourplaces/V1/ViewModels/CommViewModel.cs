﻿using Fourplaces.V1.Ressources;
using Fourplaces.V1.Services;
using Fourplaces.V1.Views;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces.V1.ViewModels
{
    class CommViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<INavigationService>   _navigationService;
        private readonly Lazy<IManagerService>      _managerService;

        // Param Navigation
        [NavigationParameter("PlaceItemSummary")]
        public PlaceItemSummary Place { get; set; }

        // Command
        public ICommand AddCommand { get; }
        public ObservableCollection<CommentItem> CommList { get; }

        // Constructeur
        public CommViewModel()
        {
            _navigationService  = new Lazy<INavigationService>(()   => DependencyService.Resolve<INavigationService>());
            _managerService     = new Lazy<IManagerService>(()      => DependencyService.Resolve<IManagerService>());
            
            CommList    = new ObservableCollection<CommentItem>();

            AddCommand  = new Command(AddCommAction);
        }

        private async void AddCommAction()
        {
            await _navigationService.Value.PushAsync<AddCommPage>(new System.Collections.Generic.Dictionary<string, object>
            {
                {"PlaceItemSummary", Place }
            });
        }

        public override void Initialize(Dictionary<string, object> navigationParameters)
        {
            base.Initialize(navigationParameters);
        }

        public override async Task OnResume()
        {
            await base.OnResume();

            // Pour le refresh pas d'id donc on clean
            CommList.Clear();

            var commList = await _managerService.Value.GetCommForPlace(Place.Id);
            foreach (var comm in commList)
            {
                CommList.Add(comm);
            }
        }
    }
}
