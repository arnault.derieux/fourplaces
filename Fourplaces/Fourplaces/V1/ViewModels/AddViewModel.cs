﻿using Fourplaces.V1.Ressources;
using Fourplaces.V1.Services;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fourplaces.V1.ViewModels
{
    class AddViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<INavigationService>   _navigationService;
        private readonly Lazy<IDialogService>       _dialogService;
        private readonly Lazy<IManagerService>      _managerService;
        private readonly Lazy<IPermissionsService> _permService;

        // Variables
        private string  _titre;
        private string  _description;
        private string  _lattitude;
        private string  _longitude; 
        private string  _labelChoose;
        private int     _idImage;
        private int     _idPhoto = 1; //Defaut
        private bool    _chooseEnable;
        private bool    _addEnable;

        // Commands
        public ICommand AddCommand { get; }
        public ICommand ChooseCommand { get; }

        // Binding
        public string LabelChoose
        {
            get => _labelChoose;
            set => SetProperty(ref _labelChoose, value);
        }
        public bool ChooseEnable
        {
            get => _chooseEnable;
            set => SetProperty(ref _chooseEnable, value);
        }
        public bool AddEnable
        {
            get => _addEnable;
            set => SetProperty(ref _addEnable, value);
        }
        public string Titre
        {
            get => _titre;
            set => SetProperty(ref _titre, value);
        }
        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }
        public string Lattitude
        {
            get => _lattitude;
            set => SetProperty(ref _lattitude, value);
        }

        public string Longitude
        {
            get => _longitude;
            set => SetProperty(ref _longitude, value);
        }

        public int IdImage
        {
            get => _idImage;
            set => SetProperty(ref _idImage, value);
        }

        // Constructeur
        public AddViewModel()
        {
            _navigationService  = new Lazy<INavigationService>(()   => DependencyService.Resolve<INavigationService>());
            _dialogService      = new Lazy<IDialogService>(()       => DependencyService.Resolve<IDialogService>());
            _managerService     = new Lazy<IManagerService>(()      => DependencyService.Resolve<IManagerService>());
            _permService        = new Lazy<IPermissionsService>(()  => DependencyService.Resolve<IPermissionsService>());

            AddCommand = new Command(AddAction);
            ChooseCommand   = new Command(ChooseAction);

            ChooseEnable    = true;
            AddEnable       = true;
            LabelChoose     = "Ajouter une photo";

            IdImage         = 0;

            _ = FetchPositionUserAsync();
        }
        private async void ChooseAction()
        {
            ChooseEnable = false;
            var result = await _dialogService.Value.DisplayAlertAsync("Choix", "Veuillez chosir entre prendre une photo ou choisir une photo depuis la galerie", "Galerie", "Photo");
            if (result)
            {
                await CrossMedia.Current.Initialize();

                bool granted = await _permService.Value.Check(new string[1] { "Storage" });

                if (granted)
                {
                    AddEnable = false;
                    LabelChoose = "Ajout en cours...";
                    MediaFile file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                    {
                        CompressionQuality = 10
                    });

                    if (file == null)
                    {
                        LabelChoose = "Ajouter une photo";
                        AddEnable = true;
                        ChooseEnable = true;
                        return;
                    }
                    _idPhoto = await _managerService.Value.PushImage(ConvertMediaFileToByteArray(file));
                    AddEnable = true;
                    LabelChoose = "Photo sélectionnée !";
                }
                else
                {
                    ChooseEnable = true;
                    await _dialogService.Value.DisplayAlertAsync("Permissions", "Permission non accordée.", "Ok");
                }
            }
            else
            {
                await CrossMedia.Current.Initialize();

                bool granted = await _permService.Value.Check(new string[2] { "Storage", "Camera" });

                if (granted)
                {

                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await _dialogService.Value.DisplayAlertAsync("Camera indisponible", "pas de caméra sur l'appareil.", "Ok");
                        return;
                    }
                    AddEnable = false;
                    LabelChoose = "Ajout en cours...";
                    MediaFile file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        Directory = "Sample",
                        Name = "test.jpg",
                        CompressionQuality = 10
                    });

                    if (file == null)
                    {
                        LabelChoose = "Ajouter une photo";
                        AddEnable = true;
                        ChooseEnable = true;
                        return;
                    }
                    _idPhoto = await _managerService.Value.PushImage(ConvertMediaFileToByteArray(file));
                    AddEnable = true;
                    LabelChoose = "Photo sélectionnée !";
                }
                else
                {
                    ChooseEnable = true;
                    await _dialogService.Value.DisplayAlertAsync("Permissions", "Permission non accordée.", "Ok");
                }
            }
        }
        private byte[] ConvertMediaFileToByteArray(MediaFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                file.GetStream().CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        private async void AddAction()
        {
            if (Titre != null && Description != null && Lattitude != null && Longitude != null)
            {
                AddEnable = false;
                ChooseEnable = false;
                CreatePlaceRequest res;
                res = new CreatePlaceRequest() { Title = Titre, Description = Description, Latitude = Convert.ToDouble(Lattitude), Longitude = Convert.ToDouble(Longitude), ImageId = _idPhoto };
                Response response = await _managerService.Value.PostNew(res);
                if (response.IsSuccess)
                {
                    await _navigationService.Value.PopAsync();
                }
                else
                {
                    AddEnable = true;
                    ChooseEnable = true;
                }
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Saisie invalide", "Veuillez vérifier vos saisies.", "Ok");
            }
        }


        // GPS
        public async Task FetchPositionUserAsync()
        {
            Lattitude = "0,1";
            Longitude = "0,1";
            bool granted = await _permService.Value.Check(new string[1] { "Location" });
            if (granted)
            {
                try
                {
                    var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                    var location = await Geolocation.GetLocationAsync(request);
                    if (location != null)
                    {
                        Lattitude = location.Latitude.ToString();
                        Longitude = location.Longitude.ToString();
                    }
                    else
                    {
                        var locationOld = await Geolocation.GetLastKnownLocationAsync();
                        if (locationOld != null)
                        {
                            Lattitude = locationOld.Latitude.ToString();
                            Longitude = locationOld.Longitude.ToString();
                        }
                    }
                }
                catch (FeatureNotEnabledException)
                {
                    await _dialogService.Value.DisplayAlertAsync("GPS inactif", "Veuillez activer le GPS pour la prochaine fois.", "Ok");
                }
                catch (PermissionException)
                {
                    await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                }
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Permissions", "Permission pour le GPS non accordée.", "Ok");
            }
        }
    }
}
