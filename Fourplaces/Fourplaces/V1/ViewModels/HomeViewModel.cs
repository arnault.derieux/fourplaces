﻿using Fourplaces.V1.Ressources;
using Fourplaces.V1.Services;
using Fourplaces.V1.Views;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces.V1.ViewModels
{
    class HomeViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<INavigationService>   _navigationService;
        private readonly Lazy<IManagerService>      _managerService;

        // Commands
        public ICommand AddCommand { get; }
        public ICommand ProfilCommand { get; }

        // Variables
        public ObservableCollection<PlaceItemSummary> PlaceList { get; set; }
        private PlaceItemSummary _selectedPlace;

        // Binding
        public PlaceItemSummary SelectedPlace
        {
            get => _selectedPlace;
            set
            {
                if (SetProperty(ref _selectedPlace, value) && value != null)
                {
                    SelectedPlaceChanged(value);
                    SelectedPlace = null;
                }
            }
        }

        private async void SelectedPlaceChanged(PlaceItemSummary value)
        {
            await _navigationService.Value.PushAsync<DetailPage>(new System.Collections.Generic.Dictionary<string, object>
            {
                {"PlaceItemSummary", value }
            });
        }

        public HomeViewModel()
        {
            _navigationService  = new Lazy<INavigationService>(()   => DependencyService.Resolve<INavigationService>());
            _managerService     = new Lazy<IManagerService>(()      => DependencyService.Resolve<IManagerService>());

            PlaceList       = new ObservableCollection<PlaceItemSummary>();

            AddCommand      = new Command(AddAction);
            ProfilCommand   = new Command(ProfilAction);
        }

        private async void ProfilAction()
        {
            await _navigationService.Value.PushAsync<ProfilPage>();
        }

        private async void AddAction()
        {
            await _navigationService.Value.PushAsync<AddPage>();
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            var placeList = await _managerService.Value.GetAllPlaces();
            var placeById = PlaceList.ToDictionary(x => x.Id, x => x);

            foreach (var place in placeList)
            {
                if (!placeById.ContainsKey(place.Id))
                {
                    // Insert -> A l'envers histoire de voir les plus récent d'abord...
                    PlaceList.Insert(0, place);
                }
            }
            
        }

    }
}
