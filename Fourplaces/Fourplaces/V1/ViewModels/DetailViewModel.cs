﻿using Fourplaces.V1.Ressources;
using Fourplaces.V1.Views;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Fourplaces.V1.ViewModels
{
    class DetailViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<INavigationService> _navigationService;

        // Param Navigation
        [NavigationParameter("PlaceItemSummary")]
        public PlaceItemSummary Place { get; set; }

        public string Title { get; set; }

        private Position _myPosition;
        public Position MyPosition { 
            get { return _myPosition; }
            set { _myPosition = value; OnPropertyChanged(); } 
        }

        private ObservableCollection<Pin> _pinCollection = new ObservableCollection<Pin>();
        public ObservableCollection<Pin> PinCollection {
            get { return _pinCollection; }
            set { _pinCollection = value; OnPropertyChanged(); }
        }

        // Commands
        public ICommand CommCommand { get; }

        // Constructeur
        public DetailViewModel()
        {
            _navigationService = new Lazy<INavigationService>(() => DependencyService.Resolve<INavigationService>());
            CommCommand = new Command(CommAction);
        }

        public override void Initialize(Dictionary<string, object> navigationParameters)
        {
            base.Initialize(navigationParameters);
            Title = Place.Title;
            MyPosition = new Position(Place.Latitude, Place.Longitude);
            PinCollection.Add(new Pin() { Position = MyPosition, Type = PinType.Generic, Label = Place.Title });
        }

        private async void CommAction()
        {
            await _navigationService.Value.PushAsync<CommPage>(new Dictionary<string, object>
            {
                {"PlaceItemSummary", Place }
            });
        }
    }
}
