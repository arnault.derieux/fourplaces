﻿using Fourplaces.V1.Ressources;
using Fourplaces.V1.Services;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces.V1.ViewModels
{
    class AddCommViewModel : ViewModelBase
    {
        // Services
        private readonly Lazy<INavigationService>   _navigationService;
        private readonly Lazy<IDialogService>       _dialogService;
        private readonly Lazy<IManagerService>      _managerService;

        // Param Navigation
        [NavigationParameter("PlaceItemSummary")]
        public PlaceItemSummary Place { get; set; }

        // Variable
        private string _text;

        // Commands
        public ICommand AddCommCommand { get; }

        // Binding
        public string Text
        {
            get => _text;
            set => SetProperty(ref _text, value);
        }

        // Constructeur
        public AddCommViewModel()
        {
            _navigationService      = new Lazy<INavigationService>(()   => DependencyService.Resolve<INavigationService>());
            _dialogService          = new Lazy<IDialogService>(()       => DependencyService.Resolve<IDialogService>());
            _managerService         = new Lazy<IManagerService>(()      => DependencyService.Resolve<IManagerService>());

            AddCommCommand = new Command(AddCommAction);
        }

        private async void AddCommAction()
        {
            if (Text != null)
            {
                CreateCommentRequest res;
                res = new CreateCommentRequest() { Text = Text };

                Response response = await _managerService.Value.AddComment(Place.Id,res);
                if (response.IsSuccess)
                {
                    await _navigationService.Value.PopAsync();
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Erreur", "Erreur lors de la requete.", "Ok");
                }
            }
            else
            {
                await _dialogService.Value.DisplayAlertAsync("Saisie invalide", "Veuillez vérifier vos saisies.", "Ok");
            }
        }
        public override void Initialize(Dictionary<string, object> navigationParameters)
        {
            base.Initialize(navigationParameters);
        }
    }
}
