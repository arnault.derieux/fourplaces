﻿using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fourplaces.V1.Services
{
    public interface IPermissionsService
    {
        void CheckAndAskAllPermAtStart();
        Task<bool> Check(string[] perm);
    }
    class PermissionsService : IPermissionsService
    {
        public PermissionsService()
        {

        }
        private readonly Permission[] _perms = new Permission[3] {
            Permission.Camera, Permission.Storage, Permission.Location
        };

        public async Task<bool> Check(string[] perm)
        {
            bool res = true;
            foreach (string _r in perm)
            {
                bool resLocal;
                switch (_r)
                {
                    case "Camera":
                        resLocal = (PermissionStatus.Granted == await CrossPermissions.Current.CheckPermissionStatusAsync(_perms[0]));
                        if(res && !resLocal)
                        {
                            res = false;
                        }
                        break;
                    case "Storage":
                        resLocal = (PermissionStatus.Granted == await CrossPermissions.Current.CheckPermissionStatusAsync(_perms[1]));
                        if (res && !resLocal)
                        {
                            res = false;
                        }
                        break;
                    case "Location":
                        resLocal = (PermissionStatus.Granted == await CrossPermissions.Current.CheckPermissionStatusAsync(_perms[2]));
                        if (res && !resLocal)
                        {
                            res = false;
                        }
                        break;
                    default:
                        res = false;
                        break;
                }
            }
            return res;
        }

        public void CheckAndAskAllPermAtStart()
        {
            CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera, Permission.Storage, Permission.Location);
        }
    }
}
