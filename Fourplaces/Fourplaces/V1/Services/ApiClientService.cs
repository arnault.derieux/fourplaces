﻿using Fourplaces.V1.Ressources;
using Newtonsoft.Json;
using Plugin.Media.Abstractions;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Fourplaces.V1.Services
{
	public interface IApiClientService
	{
		Task<HttpResponseMessage> Execute(HttpMethod method, string url, object data = null, string token = null);
		Task<T> ReadFromResponse<T>(HttpResponseMessage response);
	}
    class ApiClientService : IApiClientService
    {
		private readonly HttpClient _client = new HttpClient();
		private readonly Lazy<IDialogService> _dialogService;

		public ApiClientService()
		{
			_dialogService = new Lazy<IDialogService>(() => DependencyService.Resolve<IDialogService>());
		}

		public async Task<HttpResponseMessage> Execute(HttpMethod method, string url, object data = null, string token = null)
		{
			HttpRequestMessage request = new HttpRequestMessage(method, url);

			if (data != null)
			{
				request.Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
			}

			if (token != null)
			{
				request.Headers.Add("Authorization", $"Bearer {token}");
			}

			// Test Connectivité
			var current = Connectivity.NetworkAccess;
			
			if (current != NetworkAccess.Internet)
			{
				await _dialogService.Value.DisplayAlertAsync("Pas de connection internet.", "Veuillez activer votre connection internet pour poursuivre.", "Ok");
				return new HttpResponseMessage
				{
					StatusCode = HttpStatusCode.ServiceUnavailable
				};
			}
			else
			{
				return await _client.SendAsync(request);
			}
		}

		public async Task<T> ReadFromResponse<T>(HttpResponseMessage response)
		{
			string result = await response.Content.ReadAsStringAsync();

			return JsonConvert.DeserializeObject<T>(result);
		}
	}
}
