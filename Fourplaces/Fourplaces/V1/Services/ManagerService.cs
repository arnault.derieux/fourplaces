﻿
using Fourplaces.V1.Ressources;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;
using Storm.Mvvm.Services;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Fourplaces.V1.Services
{
    public interface IManagerService
    {
        void Connect(LoginResult data);
        Task<List<PlaceItemSummary>> GetAllPlaces();
        Task<UserItem> GetUser();
        Task<Response> PostNew(CreatePlaceRequest res);
        Task<int> PushImage(byte[] v);
        Task<Response<UserItem>> UpdateUser(UpdateProfileRequest res);
        Task<Response> UpdatePasswordUser(UpdatePasswordRequest res);
        Task<List<CommentItem>> GetCommForPlace(int id);
        Task<Response> AddComment(int id, CreateCommentRequest res);
    }

    // Super class qui sert de Manager global de l'application
    public class ManagerService : IManagerService
    {
        // Services
        private readonly Lazy<IApiClientService>    _clientService;
        private readonly Lazy<IDialogService>       _dialogService;

        // Variables
        private LoginResult             _login;
        private List<PlaceItemSummary>  _placeList;
        private UserItem                _user;
        private List<CommentItem>       _commList;

        // Constante URLs
        private const String ROOT       = "https://td-api.julienmialon.com";
        private const String PLACES     = "/places";
        private const String USER       = "/me";
        private const String PLACE      = "/places";
        private const String PASSWORD   = "/password";
        private const String COMMENTS   = "/comments";

        // Constructeur
        public ManagerService() {
            _clientService = new Lazy<IApiClientService>(()     => DependencyService.Resolve<IApiClientService>());
            _dialogService = new Lazy<IDialogService>(()        => DependencyService.Resolve<IDialogService>());
        }
        public void Connect(LoginResult data)
        {
            _login = data;
        }

        public LoginResult getToken()
        {
            return _login;
        }

        public async Task<List<PlaceItemSummary>> GetAllPlaces()
        {
            await FetchAllPlaces();
            return await Task.FromResult(_placeList);
        }

        private async Task FetchAllPlaces()
        {
            HttpResponseMessage _httpResponseMessage = await _clientService.Value.Execute(HttpMethod.Get, ROOT + PLACES, null, _login.AccessToken);
            if (_httpResponseMessage.IsSuccessStatusCode)
            {
                Response<List<PlaceItemSummary>> _res = await _clientService.Value.ReadFromResponse<Response<List<PlaceItemSummary>>>(_httpResponseMessage);
                if (_res.IsSuccess)
                {
                    _placeList = _res.Data;
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Echec de la requete", $"Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                }
            }
        }

        public async Task<UserItem> GetUser()
        {
            await FetchUser();
            return await Task.FromResult(_user);
        }

        private async Task FetchUser()
        {
            HttpResponseMessage _httpResponseMessage = await _clientService.Value.Execute(HttpMethod.Get, ROOT + USER, null, _login.AccessToken);
            if (_httpResponseMessage.IsSuccessStatusCode)
            {
                Response<UserItem> _res = await _clientService.Value.ReadFromResponse<Response<UserItem>>(_httpResponseMessage);
                if (_res.IsSuccess)
                {
                    _user = _res.Data;
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Echec de la requete", $"Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                }
            }
        }

        public async Task<Response> PostNew(CreatePlaceRequest res)
        {
            HttpResponseMessage _httpResponseMessage = await _clientService.Value.Execute(HttpMethod.Post, ROOT + PLACE, res, _login.AccessToken);
            if (_httpResponseMessage.IsSuccessStatusCode)
            {
                Response _res = await _clientService.Value.ReadFromResponse<Response>(_httpResponseMessage);
                if (_res.IsSuccess)
                {
                    await _dialogService.Value.DisplayAlertAsync("Ajout réussi", "Ajout réussi dans l'api.", "Ok");
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Echec de la connection", $"Connection échouée. Veuillez vérifier vos saisies et réessayer. Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                }
                return _res;
            }
            else
            {
                return new Response()
                {
                    ErrorCode = "503",
                    ErrorMessage = "ServiceUnavailable",
                    IsSuccess = false
                };
            }
        }

        public async Task<int> PushImage(byte[] data)
        {
            HttpClient client = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://td-api.julienmialon.com/images");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _login.AccessToken);
            MultipartFormDataContent requestContent = new MultipartFormDataContent();
            var imageContent = new ByteArrayContent(data);
            imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
            requestContent.Add(imageContent, "file", "file.jpg");
            request.Content = requestContent;
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();
                int id = 0;
                if (response.IsSuccessStatusCode)
                {
                    Response<ImageItem> res = JsonConvert.DeserializeObject<Response<ImageItem>>(result);
                    if (res.IsSuccess)
                    {
                        id = res.Data.Id;
                    }
                }
                else
                {
                    Console.WriteLine(response.StatusCode);
                }
                return id;
            }
            return 1;
        }

        public async Task<Response<UserItem>> UpdateUser(UpdateProfileRequest res)
        {
            HttpResponseMessage _httpResponseMessage = await _clientService.Value.Execute(new HttpMethod("PATCH"), ROOT + USER, res, _login.AccessToken);
            if (_httpResponseMessage.IsSuccessStatusCode)
            {
                Response<UserItem> _res = await _clientService.Value.ReadFromResponse<Response<UserItem>>(_httpResponseMessage);
                if (_res.IsSuccess)
                {
                    await _dialogService.Value.DisplayAlertAsync("Edition réussi", "Edition réussie dans l'api.", "Ok");
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Echec de la connection", $"Connection échouée. Veuillez vérifier vos saisies et réessayer. Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                }
                return _res;
            }
            else
            {
                return new Response<UserItem>()
                {
                    ErrorCode       = "503",
                    ErrorMessage    = "ServiceUnavailable",
                    IsSuccess       = false,
                    Data            = new UserItem()
                    {
                        Email       = "default",
                        FirstName   = "default",
                        LastName    = "default",
                        ImageId     = 0,
                        Id          = 0
                    }
                };
            }
        }

        public async Task<Response> UpdatePasswordUser(UpdatePasswordRequest res)
        {
            HttpResponseMessage _httpResponseMessage = await _clientService.Value.Execute(new HttpMethod("PATCH"), ROOT + USER + PASSWORD, res, _login.AccessToken);
            if (_httpResponseMessage.IsSuccessStatusCode)
            {
                Response _res = await _clientService.Value.ReadFromResponse<Response>(_httpResponseMessage);
                if (_res.IsSuccess)
                {
                    await _dialogService.Value.DisplayAlertAsync("Edition réussi", "Edition réussie dans l'api.", "Ok");
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Echec de la connection", $"Connection échouée. Veuillez vérifier vos saisies et réessayer. Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                }
                return _res;
            }
            else
            {
                return new Response()
                {
                    ErrorCode = "503",
                    ErrorMessage = "ServiceUnavailable",
                    IsSuccess = false
                };
            }
        }
        public async Task<List<CommentItem>> GetCommForPlace(int id)
        {
            await FetchAllComm(id);
            return await Task.FromResult(_commList);
        }

        public async Task FetchAllComm(int id)
        {
            HttpResponseMessage _httpResponseMessage = await _clientService.Value.Execute(HttpMethod.Get, ROOT + PLACES +"/" + id, null, _login.AccessToken);
            if (_httpResponseMessage.IsSuccessStatusCode)
            {
                Response<PlaceItem> _res = await _clientService.Value.ReadFromResponse<Response<PlaceItem>>(_httpResponseMessage);
                if (_res.IsSuccess)
                {
                    _commList = _res.Data.Comments;
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Echec de la requete", $"Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                }
            }
        }

        public async Task<Response> AddComment(int id, CreateCommentRequest res)
        {
            HttpResponseMessage _httpResponseMessage = await _clientService.Value.Execute(HttpMethod.Post, ROOT + PLACES + "/" + id + COMMENTS, res, _login.AccessToken);
            if (_httpResponseMessage.IsSuccessStatusCode)
            {
                Response _res = await _clientService.Value.ReadFromResponse<Response>(_httpResponseMessage);
                if (_res.IsSuccess)
                {
                    await _dialogService.Value.DisplayAlertAsync("Ajout réussi", "Ajout réussi dans l'api.", "Ok");
                }
                else
                {
                    await _dialogService.Value.DisplayAlertAsync("Echec de la requete", $"Code erreur : { _res.ErrorCode }. { _res.ErrorMessage }", "Ok");
                }
                return _res;
            }
            else
            {
                return new Response()
                {
                    ErrorCode = "503",
                    ErrorMessage = "ServiceUnavailable",
                    IsSuccess = false
                };
            }
        }
    }
}
