﻿using Fourplaces.V1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fourplaces.V1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditProfilPage : ContentPage
    {
        public EditProfilPage()
        {
            BindingContext = new EditProfilViewModel();
            InitializeComponent();
        }
    }
}