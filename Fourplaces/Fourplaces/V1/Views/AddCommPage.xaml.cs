﻿using Fourplaces.V1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fourplaces.V1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddCommPage : ContentPage
    {
        public AddCommPage()
        {
            BindingContext = new AddCommViewModel();
            InitializeComponent();
        }
    }
}