﻿using Fourplaces.V1.ViewModels;
using Storm.Mvvm.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fourplaces.V1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : BaseContentPage
    {
        public HomePage()
        {
            BindingContext = new HomeViewModel();
            InitializeComponent();
        }
    }
}