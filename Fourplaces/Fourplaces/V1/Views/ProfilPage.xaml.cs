﻿using Fourplaces.V1.ViewModels;
using Storm.Mvvm.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fourplaces.V1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilPage : BaseContentPage
    {
        public ProfilPage()
        {
            BindingContext = new ProfilViewModel();
            InitializeComponent();
        }
    }
}