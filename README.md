# FourPlaces
- Université d'Orléans - **Arnault Derieux**
- Master 1 IMIS - 15/02/2020
- Développement multi-plateforme

### Avancement :
- **Toutes les fonctionnalités obligatoires et bonus.**
- Code découpé en services et selon une arborescence d'un TP (dossiers, nommage...)
- **Gestion des permissions**
	- Check/Ask lors du Login
		- Si Ok -> Fonctionnalités activées
		- Sinon -> Message d'erreur lors de la demande de la fonctionnalité
	- **Manifest Android**
		- Api Key pour la Map (non signé car en mode debug)
- **Map** : Fonctionne sous Android -> *Aucune idée* pour IOS (pas de test possible)
	- Gestion de la Map : Class trouvée sur Internet qui permet le binding
		- https://xamarinhelp.com/xamarin-forms-maps/

> Dépôt Gitlab du projet Fourplaces
